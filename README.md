# Discuz! 官方模板 X系列仿7.2风格

## 维护说明

**本模板自 2021 年 3 月开始由 Discuz! 应用中心团队与 Discuz! X 社区开发者团队共同维护，您可以在我们的 Gitee 仓库 https://gitee.com/discuzaddons/comsenz_x72/ 上向我们反馈模板 Bug 。**

## 模板说明

本模板是基于 Discuz! X 版本制作的 Discuz! 7.2 风格模版，目前已完成 X3.0 - X3.5 适配工作，诚邀喜爱复古的您体验选用。

## 图片展示

![站点首页](.readme/01.jpg)

![主题列表](.readme/02.jpg)

![主题内容](.readme/03.jpg)

![个人资料](.readme/04.jpg)